// Copyright (c) 2022 Aiden Baker
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

"use strict";

import{addCommand}from'@cumcord/commands';
import{getNeko}from'./api.js';
export default(data)=>{
    let command;
    return{
        onLoad(){
            const removeCommand=cumcord.commands.addCommand({
                name:"neko",
                description:"Send images from nekos.life",
                args:[{
                    name:"type",
                    description:"The type of image to get",
                    type:"string",
                    required:true
                }],
                handler:(ctx,send)=>{
                    return`${getNeko(ctx.args.type)}`;
                }
            })
        },
        onunload(){
        }
    }
}